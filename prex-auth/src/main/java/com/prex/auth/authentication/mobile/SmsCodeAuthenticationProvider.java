package com.prex.auth.authentication.mobile;

import com.prex.auth.security.PrexUserDetailsServiceImpl;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @Classname SmsCodeAuthenticationProvider
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-07-08 11:49
 * @Version 1.0
 */
public class SmsCodeAuthenticationProvider implements AuthenticationProvider {

    private PrexUserDetailsServiceImpl userDetailService;

    @Override
    public Authentication authenticate(Authentication authentication) {
        SmsCodeAuthenticationToken authenticationToken = (SmsCodeAuthenticationToken) authentication;
        String mobile = (String) authenticationToken.getPrincipal();
        //调用自定义的userDetailsService认证 通过手机号来查询
        UserDetails userDetails = userDetailService.loadUserByMobile(mobile);
        //如果user不为空重新构建SmsCodeAuthenticationToken（已认证）
        SmsCodeAuthenticationToken authenticationResult = new SmsCodeAuthenticationToken(userDetails, userDetails.getAuthorities());
        authenticationResult.setDetails(authenticationToken.getDetails());

        return authenticationResult;
    }

    /**
     * 只有Authentication为SmsCodeAuthenticationToken使用此Provider认证
     * @param aClass
     * @return
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return SmsCodeAuthenticationToken.class.isAssignableFrom(aClass);
    }

    public PrexUserDetailsServiceImpl getUserDetailService() {
        return userDetailService;
    }

    public void setUserDetailService(PrexUserDetailsServiceImpl userDetailService) {
        this.userDetailService = userDetailService;
    }

}